import express from "express"
import session from "express-session"

const app = express()
/*
	Session Middleware.
	ACHTUNG: per default wird die Session im RAM gespeichert. Dies ist nur zu
	Testzwecken zu verwenden. Üblicher Weise wird die Session mit Hilfe einer DB
	verwaltet.
*/
app.use(session({
	secret: "fjk32498hjklasdf",
	resave: false,
	saveUninitialized: true,
	// ACHTUNG: nur im Developer Modus auf false setzen. Produktiv
	// MUSS secure auf true sein (dh. https ist Voraussetzung um das Cookie zu setzen)
	cookie: { secure: false}
}))

app.get("/", (req, res) => {
	// In der Session trackt die Variable views die Aufrufe dieser Route
	if (req.session.views) {
		req.session.views++
		res.send(`Anzahl der Aufrufe bisher: ${req.session.views}`)
	} else {
		// Seite wurde noch nicht aufgerufen, wir initialisieren die views Variable in der Session
		req.session.views = 1
		res.send("Hallo, dies ist der Erste Aufruf in dieser Sitzung.")
	}
})

app.listen(3000, () => console.log("Server läuft unter http://localhost:3000")
)